#!/usr/bin/env bash

#==============================================
#title           :Tor bridge installer - Debian
#==============================================
CODENAME=$(lsb_release -cs)
NICKNAME=$(hostname)

echo "Updating..."
apt update && apt upgrade -y

echo "Configuring tor deb repo..."
apt install gnupg gpg apt-transport-https lsb-release unattended-upgrades apt-listchanges -y

cat <<EOF > /etc/apt/sources.list.d/tor.list
   deb     [signed-by=/usr/share/keyrings/tor-archive-keyring.gpg] https://deb.torproject.org/torproject.org $CODENAME main
   deb-src [signed-by=/usr/share/keyrings/tor-archive-keyring.gpg] https://deb.torproject.org/torproject.org $CODENAME main
EOF

echo "downloading the deb repo keyring..."
wget -qO- https://deb.torproject.org/torproject.org/A3C4F0F979CAA22CDBA8F512EE8CBC9E886DDD89.asc | gpg --dearmor | tee /usr/share/keyrings/tor-archive-keyring.gpg >/dev/null

# Enable backports to get obfs4proxy package
cat <<EOF >> /etc/apt/sources.list
deb http://deb.debian.org/debian $CODENAME-backports main contrib non-free
deb-src http://deb.debian.org/debian $CODENAME-backports main contrib non-free
EOF

echo "Update and install tor, tor-keyring, and vnstat"
apt update && apt install tor tor-geoipdb vnstat deb.torproject.org-keyring -y

echo "Install obfs4proxy from backports"
apt install obfs4proxy -t $CODENAME-backports

# create new torrc
cat <<EOF > /etc/tor/torrc
BridgeRelay 1
ORPort 127.0.0.1:auto
AssumeReachable 1
ServerTransportPlugin obfs4 exec /usr/bin/obfs4proxy
ServerTransportListenAddr obfs4 0.0.0.0:54535
ExtORPort auto
ContactInfo gus@riseup.net
Log notice file /var/log/tor/notices.log
BridgeDistribution settings
Nickname $NICKNAME
EOF

echo "Restart Tor service..."
service tor restart

## Configure automatic updates
cat <<EOF > /etc/apt/apt.conf.d/50unattended-upgrades
Unattended-Upgrade::Origins-Pattern {
    "origin=Debian,codename=${distro_codename},label=Debian-Security";
    "origin=TorProject";
};
Unattended-Upgrade::Package-Blacklist {
};
Unattended-Upgrade::Automatic-Reboot "true";
EOF

cat <<EOF > /etc/apt/apt.conf.d/20auto-upgrades
APT::Periodic::Update-Package-Lists "1";
APT::Periodic::AutocleanInterval "5";
APT::Periodic::Unattended-Upgrade "1";
APT::Periodic::Verbose "1";
EOF

echo "Done!"
