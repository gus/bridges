#!/usr/bin/env bash

#===========================================================
#        Tor webtunnel bridge installer - Debian 
#===========================================================

# Usage: ./webtunnel.sh --domain example.org --contact contact@example.org --nickname bridgenickname

# Parse command-line arguments
while [[ "$#" -gt 0 ]]; do
    case $1 in
        --domain) SERVER_ADDRESS="$2"; shift ;;
        --contact) CONTACT_INFO="$2"; shift ;;
        --nickname) BRIDGE_NICKNAME="$2"; shift ;;
        -h|--help)
            echo "Usage: $0 --domain DOMAIN --contact CONTACT_EMAIL --nickname NICKNAME"
            exit 0
            ;;
        *) echo "Unknown parameter passed: $1"; exit 1 ;;
    esac
    shift
done

# Validate required parameters
if [[ -z "${SERVER_ADDRESS}" || -z "${CONTACT_INFO}" || -z "${BRIDGE_NICKNAME}" ]]; then
    echo "Error: Missing required parameters."
    echo "Usage: $0 --domain DOMAIN --contact CONTACT_EMAIL --nickname NICKNAME"
    exit 1
fi

SECRET_PATH=$(cat /dev/urandom | tr -cd 'qwertyuiopasdfghjklzxcvbnmMNBVCXZLKJHGFDSAQWERTUIOP0987654321' | head -c 24)

echo "Updating the system..."
apt update && apt upgrade -y

echo "Installing NGINX and other dependencies"
apt install apt-transport-https curl nginx socat cron vnstat htop -y

# stop nginx to install the certificate
service nginx stop

echo "Install acme and get a new certificate"
curl https://get.acme.sh | sh -s email="$CONTACT_INFO"
~/.acme.sh/acme.sh --issue --standalone --domain "$SERVER_ADDRESS"

echo "Enable new site"
cat <<EOF > /etc/nginx/sites-enabled/$SERVER_ADDRESS
server {
    listen 80;
    listen [::]:80;
    root /var/www/html;

    # Add index.php to the list if you are using PHP
    index index.html index.htm index.nginx-debian.html;

    server_name $SERVER_ADDRESS;

    location / {
        # First attempt to serve request as file, then
        # as directory, then fall back to displaying a 404.
        try_files \$uri \$uri/ =404;
    }
}

server {
    listen [::]:443 ssl http2;
    listen 443 ssl http2;
    server_name $SERVER_ADDRESS;
    #ssl on;
    ssl_certificate /root/.acme.sh/${SERVER_ADDRESS}_ecc/fullchain.cer;
    ssl_certificate_key /root/.acme.sh/${SERVER_ADDRESS}_ecc/${SERVER_ADDRESS}.key;
    ssl_session_timeout 15m;

    ssl_protocols TLSv1.2 TLSv1.3;

    ssl_ciphers ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384;

    ssl_prefer_server_ciphers off;

    ssl_session_cache shared:MozSSL:50m;
    #ssl_ecdh_curve secp521r1,prime256v1,secp384r1;
    ssl_session_tickets off;

    add_header Strict-Transport-Security "max-age=63072000" always;

    location = /$SECRET_PATH {
        proxy_pass http://127.0.0.1:15000;
        proxy_http_version 1.1;

        ### Set WebSocket headers ###
        proxy_set_header Upgrade \$http_upgrade;
        proxy_set_header Connection "upgrade";

        ### Set Proxy headers ###
        proxy_set_header        Accept-Encoding   "";
        proxy_set_header        Host            \$host;
        proxy_set_header        X-Real-IP       \$remote_addr;
        proxy_set_header        X-Forwarded-For \$proxy_add_x_forwarded_for;
        proxy_set_header        X-Forwarded-Proto \$scheme;
        add_header              Front-End-Https   on;

        proxy_redirect off;
        access_log  off;
        error_log off;
    }
}
EOF

echo "Restarting NGINX"
service nginx restart

echo "Installing Docker"
curl -fsSL https://get.docker.com -o get-docker.sh
sh ./get-docker.sh

truncate --size 0 .env
cat <<EOF > .env
URL=https://$SERVER_ADDRESS/$SECRET_PATH
OPERATOR_EMAIL=$CONTACT_INFO
EOF
echo "BRIDGE_NICKNAME=${BRIDGE_NICKNAME}" >> .env
echo "GENEDORPORT=4$(cat /dev/urandom | tr -cd '0987654321'|head -c 4)" >> .env

curl -L https://gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/webtunnel/-/raw/main/release/container/docker-compose.yml?inline=false > docker-compose.yml

echo "Starting Docker..."
docker compose up -d

# Add a cron job for certificate renewal
echo "Setting up cron job for automatic certificate renewal"
(crontab -l 2>/dev/null; echo "0 0 * * 0 service nginx stop && ~/.acme.sh/acme.sh --renew --domain $SERVER_ADDRESS --ecc && service nginx start") | crontab -

echo "Your Webtunnel Bridge line:"
echo "$(docker compose exec webtunnel-bridge get-bridge-line.sh)"
echo "Or copy and paste: \$ docker compose exec webtunnel-bridge get-bridge-line.sh"